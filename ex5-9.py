import turtle

def bar(a,height):

        a.begin_fill()
        a.left(90)
        a.forward(height)
        a.right(90)
        a.forward(3)
        a.right(90)
        a.forward(height)
        a.left(90)
        a.forward(1)
        a.end_fill()

wn=turtle.Screen()
wn.bgcolor("green")

tess=turtle.Turtle()

xs=[-100, -10, -35, -48, -124]

for i in xs:
	bar(tess,i)

turtle.mainloop()

