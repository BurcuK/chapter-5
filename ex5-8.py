import turtle

def bar(a,height):

	a.begin_fill()
	a.left(90)
	a.forward(height)
	a.right(90)
	a.forward(3)
	a.right(90)
	a.forward(height)
	a.left(90)
	a.forward(1)
	a.end_fill()

wn=turtle.Screen()
wn.bgcolor("green")

tess=turtle.Turtle()

xs=[48,117,200,100,160,260,220]

for i in xs:
        if i>=200:
                tess.color("red")
        elif 200>i>=100:
                tess.color("yellow")
        elif 100>i:
                tess.color("lightgreen")
	bar(tess,i)

turtle.mainloop()


        
