import turtle

def bar_chart(a,height):
	a.begin_fill()
	a.left(90)
	a.forward(height)
	a.right(90)
	a.forward(3)
	a.right(90)
	a.forward(height)
	a.left(90)
	a.forward(1)
	a.end_fill()

wn=turtle.Screen()
wn.bgcolor("green")

tess=turtle.Turtle()
tess.color("lightblue", "lightgreen")

xs=[48,117,200,240,160,260,220]

for i in xs:
    bar_chart(tess,i)

turtle.mainloop()
	
